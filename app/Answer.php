<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //
    protected $fillable = ['name', 'city', 'author', 'month', 'return_date', 'book'];

}
