<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function gallery(){
        return view('gallery');
    }

    public function showCharts(){
        return view('charts');
    }

    public function dataChart(){
        $data=[];
        for($i=1;$i<12;$i++){
            $cur_data = $this->getCount($i);
            array_push($data, $cur_data);
        }
        return[
            'labels'=>['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            'datasets'=>array([
                'label'=>'Взято книг',
                'backgroundColor'=>['#ff3344'],
                'color'=>'#f55f8a',
                'data'=>$data,
            ],
            )
        ];
    }

    public function getCount($month){
        $needlyMonth = Answer::where('month',$month)->count();

        return $needlyMonth;
    }

}
