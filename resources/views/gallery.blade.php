@extends('layouts.app')

@section('content')

    <div class="container">
        <h2>Советуем прочитать:</h2>

        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div>
                        <img src="{{asset('/img/duglas.jpg')}}" class="gallery_img">
                    </div>
                    <span> <b>Дуглас Аддамс</b><i> Автостопом по галактике</i> </span>
                </div>
                <div class="carousel-item">
                    <div>

                        <img src="{{asset('/img/tolkin.jpg')}}" class="gallery_img">
                    </div>
                    <span> <b>Дж. Р. Толкиен</b><i> Властелин колец</i> </span>
                </div>
                <div class="carousel-item">
                            <div>

                                <img src="{{asset('/img/azimov.jpg')}}" class="gallery_img">
                            </div>
                    <span> <b>Айзек Азимов</b><i> Акадения</i> </span>
                </div>
                <div class="carousel-item">
                    <div>

                        <img src="{{asset('/img/gordon.jpg')}}" class="gallery_img">
                    </div>
                    <span> <b>Джеймс Гордон</b><i> Конструкции, или почему не ломаются вещи?</i> </span>
                </div>
                <div class="carousel-item">
                    <div>

                        <img src="{{asset('/img/franklin.jpg')}}" class="gallery_img">
                    </div>
                    <span> <b>Уолтер Айзексон</b><i> Бенджамин Франклин</i> </span>
                </div>
                <div class="carousel-item">
                    <div>

                        <img src="{{asset('/img/tesla.jpg')}}" class="gallery_img">
                    </div>
                    <span> <b>Бернард Карлсон</b><i> Тесла: изобретатель эпохи электричества</i> </span>
                </div>
                <div class="carousel-item">
                    <div>

                        <img src="{{asset('/img/hainal.jpg')}}" class="gallery_img">
                    </div>
                    <span> <b>Роберт Энсон Хайнлайн</b><i> Луна — суровая хозяйка</i> </span>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>









    </div>

@endsection
